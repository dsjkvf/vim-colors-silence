" vim: set nowrap:

" HEADER

" Description:  Vim color scheme, silence.vim
" Author:       dsjkvf (dsjkvf@gmail.com)
" Maintainer:   dsjkvf (dsjkvf@gmail.com)
" Webpage:      https://bitbucket.org/dsjkvf/vim-colors-silence
" License:      MIT
" Version:      0.3

" MAIN

" Startup
hi clear
if exists("syntax_on")
    syntax reset
endif
let colors_name = "silence"

" Settings
" if no italics setting is forced via the global configuration variable
if !exists('g:silence_italic')
    " check if the terminfo entry declares support for italics
    if &termguicolors
        let g:silence_italic = 'italic'
    else
        let g:silence_italic = 'NONE'
    endif
endif

" Interface
hi Normal         cterm=NONE      ctermbg=000        ctermfg=250        gui=NONE      guibg=#000000       guifg=#bcbcbc
set background=dark
hi ColorColumn    cterm=NONE      ctermbg=059        ctermfg=NONE       gui=NONE      guibg=#303030       guifg=NONE
hi Conceal        cterm=NONE      ctermbg=NONE       ctermfg=bg         gui=NONE      guibg=NONE          guifg=bg
" hi Cursor         cterm=NONE      ctermbg=226        ctermfg=bg         gui=NONE      guibg=#87ffaf       guifg=bg
hi Cursor         cterm=NONE      ctermbg=208        ctermfg=015        gui=NONE      guibg=#ff9955       guifg=#ffffff
hi CursorColumn   cterm=NONE      ctermbg=238        ctermfg=NONE       gui=NONE      guibg=#444444       guifg=NONE
hi CursorLine     cterm=NONE      ctermbg=238        ctermfg=NONE       gui=NONE      guibg=#444444       guifg=NONE
hi CursorLineNr   cterm=NONE      ctermbg=NONE       ctermfg=015        gui=NONE      guibg=NONE          guifg=#ffffff
hi Directory      cterm=bold      ctermbg=NONE       ctermfg=015        gui=bold      guibg=NONE          guifg=#ffffff
hi EndOfBuffer    cterm=NONE      ctermbg=NONE       ctermfg=bg         gui=NONE      guibg=NONE          guifg=bg
hi ErrorMsg       cterm=NONE      ctermbg=196        ctermfg=015        gui=NONE      guibg=#ff0000       guifg=#ffffff
" hi ErrorMsg       cterm=NONE      ctermbg=088        ctermfg=015        gui=NONE      guibg=#800000       guifg=NONE
hi FoldColumn     cterm=NONE      ctermbg=NONE       ctermfg=240        gui=NONE      guibg=NONE          guifg=#585858
hi Folded         cterm=NONE      ctermbg=236        ctermfg=NONE       gui=NONE      guibg=#303030       guifg=NONE
hi LineNr         cterm=NONE      ctermbg=NONE       ctermfg=240        gui=NONE      guibg=NONE          guifg=#585858
hi MatchParen     cterm=NONE      ctermbg=NONE       ctermfg=015        gui=NONE      guibg=NONE          guifg=#ffffff
hi ModeMsg        cterm=NONE      ctermbg=NONE       ctermfg=015        gui=NONE      guibg=NONE          guifg=#ffffff
hi MoreMsg        cterm=NONE      ctermbg=NONE       ctermfg=026        gui=NONE      guibg=NONE          guifg=#1f5bde
hi NonText        cterm=NONE      ctermbg=NONE       ctermfg=81         gui=NONE      guibg=NONE          guifg=#00ffff
hi Pmenu          cterm=NONE      ctermbg=153        ctermfg=bg         gui=NONE      guibg=#afd5ff       guifg=bg
hi PmenuSel       cterm=NONE      ctermbg=026        ctermfg=015        gui=NONE      guibg=#1f5bde       guifg=#ffffff
hi PmenuSbar      cterm=NONE      ctermbg=015        ctermfg=NONE       gui=NONE      guibg=#ffffff       guifg=bg
hi PmenuThumb     cterm=NONE      ctermbg=026        ctermfg=NONE       gui=NONE      guibg=#1f5bde       guifg=NONE
hi Question       cterm=NONE      ctermbg=NONE       ctermfg=121        gui=NONE      guibg=NONE          guifg=#87ffaf
hi Search         cterm=NONE      ctermbg=026        ctermfg=015        gui=NONE      guibg=#1f5bde       guifg=#ffffff
" hi CurSearch
hi IncSearch      cterm=NONE      ctermbg=015        ctermfg=026        gui=NONE      guibg=#ffffff       guifg=#1f5bde
hi SignColumn     cterm=NONE      ctermbg=NONE       ctermfg=121        gui=NONE      guibg=NONE          guifg=#87ffaf
hi SpecialKey     cterm=NONE      ctermbg=NONE       ctermfg=81         gui=NONE      guibg=NONE          guifg=#00ffff
hi StatusLine     cterm=NONE      ctermbg=fg         ctermfg=bg         gui=NONE      guibg=fg            guifg=bg
hi StatusLineNC   cterm=NONE      ctermbg=240        ctermfg=bg         gui=NONE      guibg=#585858       guifg=bg
hi TabLine        cterm=NONE      ctermbg=240        ctermfg=bg         gui=NONE      guibg=#585858       guifg=bg
hi TabLineFill    cterm=NONE      ctermbg=240        ctermfg=bg         gui=NONE      guibg=#585858       guifg=bg
hi TabLineSel     cterm=NONE      ctermbg=fg         ctermfg=bg         gui=NONE      guibg=fg            guifg=bg
hi Title          cterm=bold      ctermbg=NONE       ctermfg=015        gui=bold      guibg=NONE          guifg=#ffffff
hi VertSplit      cterm=NONE      ctermbg=NONE       ctermfg=240        gui=NONE      guibg=NONE          guifg=#585858
hi Visual         cterm=NONE      ctermbg=153        ctermfg=bg         gui=NONE      guibg=#afd5ff       guifg=bg
hi WarningMsg     cterm=bold      ctermbg=NONE       ctermfg=196        gui=bold      guibg=NONE          guifg=#ff0000
" hi WarningMsg     cterm=bold      ctermbg=NONE       ctermfg=088        gui=bold      guibg=NONE          guifg=#800000
hi WildMenu       cterm=NONE      ctermbg=026        ctermfg=015        gui=NONE      guibg=#1f5bde       guifg=#ffffff
hi Underlined     cterm=underline ctermbg=NONE       ctermfg=015        gui=underline guibg=NONE          guifg=#ffffff

" Syntax
hi Comment        cterm=NONE      ctermbg=NONE       ctermfg=244        gui=italic    guibg=NONE          guifg=#808080      |      execute "hi Comment cterm=" . g:silence_italic
hi String         cterm=NONE      ctermbg=NONE       ctermfg=067        gui=NONE      guibg=NONE          guifg=#5f87af
hi Todo           cterm=NONE      ctermbg=121        ctermfg=bg         gui=NONE      guibg=#87ffaf       guifg=bg
hi! def link Boolean                                String
hi! def link Float                                  String
hi! def link Number                                 String
hi! def link Constant                               String
hi! def link Function                               Normal
hi! def link Identifier                             Normal
hi! def link PreProc                                Normal
hi! def link Special                                Normal
hi! def link Statement                              Normal
hi! def link Type                                   Normal
hi! def link StatusLineTerm                         StatusLine
hi! def link StatusLineTermNC                       StatusLineNC

" for Diary
hi diarySets      cterm=NONE      ctermbg=NONE       ctermfg=066        gui=NONE      guibg=NONE          guifg=#5f8787
hi diaryNote      cterm=NONE      ctermbg=NONE       ctermfg=146        gui=NONE      guibg=NONE          guifg=#afafd7

" for Diff
hi diffFile       cterm=NONE      ctermbg=NONE       ctermfg=146        gui=NONE      guibg=NONE          guifg=#afafd7
hi diffNewFile    cterm=NONE      ctermbg=NONE       ctermfg=146        gui=NONE      guibg=NONE          guifg=#afafd7
hi diffLine       cterm=NONE      ctermbg=NONE       ctermfg=015        gui=NONE      guibg=NONE          guifg=#ffffff
hi diffIndexLine  cterm=NONE      ctermbg=NONE       ctermfg=015        gui=NONE      guibg=NONE          guifg=#ffffff
hi diffAdded      cterm=NONE      ctermbg=NONE       ctermfg=121        gui=NONE      guibg=NONE          guifg=#87ffaf
hi! def link diffRemoved                            String

" for Diff mode
hi DiffDelete     cterm=NONE      ctermbg=067        ctermfg=bg         gui=NONE      guibg=#5f87af       guifg=bg
hi DiffAdd        cterm=NONE      ctermbg=121        ctermfg=bg         gui=NONE      guibg=#87ffaf       guifg=bg
" hi DiffChange     cterm=NONE      ctermbg=067        ctermfg=bg         gui=NONE      guibg=#5f87af       guifg=bg
" hi DiffText       cterm=NONE      ctermbg=121        ctermfg=bg         gui=NONE      guibg=#87ffaf       guifg=bg
" https://vi.stackexchange.com/q/5687/5232
hi DiffChange     cterm=NONE      ctermbg=146        ctermfg=bg         gui=NONE      guibg=#afafd7       guifg=bg
hi DiffText       cterm=NONE      ctermbg=015        ctermfg=bg         gui=NONE      guibg=#ffffff       guifg=bg
hi DiffCommit     cterm=bold      ctermbg=NONE       ctermfg=196        gui=bold      guibg=NONE          guifg=#ff0000
hi! def link DiffCommitFile                         DiffCommit

" for Help
hi! def link helpHyperTextJump                      String

" for HTML
hi! def link htmlLink                               Normal
hi! def link htmlComment                            Comment
hi! def link htmlCommentPart                        Comment

" for Javascript
hi! def link jsGlobalObjects                        Normal

" for man
hi! def link manTitle                               Title
hi! def link manSectionHeading                      Title
hi! def link manSubHeading                          Title
hi! def link manOptionDesc                          String
hi! def link manLongOptonDesc                       String
hi! def link manReference                           String
hi! def link manCFuncDefinition                     String

" for Markdown
hi mkdBlockquote  cterm=NONE      ctermbg=NONE       ctermfg=246        gui=italic    guibg=NONE          guifg=#949494      |       execute "hi mkdBlockquote cterm=" . g:silence_italic
hi! def link markdownHeadingDelimiter               Title
hi! def link markdownHeadingRule                    Title
hi! def link markdownLinkText                       Normal
hi! def link mkdCode                                String
hi! def link mkdComment                             Comment
hi! def link mkdLink                                String
hi! def link mkdURL                                 FoldColumn
hi! def link mkdInlineURL                           String

" for Mail
hi mailQuoted6    cterm=NONE      ctermbg=NONE       ctermfg=236        gui=italic    guibg=NONE          guifg=#303030      |       execute "hi mailQuoted4 cterm=" . g:silence_italic
hi mailQuoted5    cterm=NONE      ctermbg=NONE       ctermfg=238        gui=italic    guibg=NONE          guifg=#444444      |       execute "hi mailQuoted4 cterm=" . g:silence_italic
hi mailQuoted4    cterm=NONE      ctermbg=NONE       ctermfg=240        gui=italic    guibg=NONE          guifg=#585858      |       execute "hi mailQuoted4 cterm=" . g:silence_italic
hi mailQuoted3    cterm=NONE      ctermbg=NONE       ctermfg=242        gui=italic    guibg=NONE          guifg=#6c6c6c      |       execute "hi mailQuoted3 cterm=" . g:silence_italic
hi mailQuoted2    cterm=NONE      ctermbg=NONE       ctermfg=244        gui=italic    guibg=NONE          guifg=#808080      |       execute "hi mailQuoted2 cterm=" . g:silence_italic
hi mailQuoted1    cterm=NONE      ctermbg=NONE       ctermfg=246        gui=italic    guibg=NONE          guifg=#949494      |       execute "hi mailQuoted1 cterm=" . g:silence_italic
hi! def link mailEmail                              String
hi! def link mailURL                                String
hi! def link mailHeader                             Normal
hi! def link mailHeaderKey                          Title
hi! def link mailSubject                            Title
hi! def link mailSignature                          Comment

" for Spelling
hi SpellBad       cterm=underline ctermbg=NONE       ctermfg=NONE       gui=underline guibg=NONE          guifg=NONE
hi SpellCap       cterm=underline ctermbg=NONE       ctermfg=NONE       gui=underline guibg=NONE          guifg=NONE
hi SpellLocal     cterm=underline ctermbg=NONE       ctermfg=NONE       gui=underline guibg=NONE          guifg=NONE
hi SpellRare      cterm=underline ctermbg=NONE       ctermfg=NONE       gui=underline guibg=NONE          guifg=NONE

" for Startify
hi! def link StartifyPath                           Comment
hi! def link StartifySection                        Title

" Commands
command! SilenceMorning  hi Normal ctermfg=253 guifg=#dcdcdc
command! SilenceDay      hi Normal ctermfg=250 guifg=#bcbcbc
command! SilenceEvening  hi Normal ctermfg=248 guifg=#acacac

" APPENDIX

" Colors used (and their terminal256.palette counterparts)
"
" according to https://github.com/dsalychev/rgb2pal:
"
" 000000 -> 000 #000000
" ffffff -> 015 #ffffff
"
" 303030 -> 236 #303030
" 1f5bde -> 026 #005fd7 // the color of a highlighted item in Finder
" 444444 -> 238 #444444
" 585858 -> 240 #585858
" 5f8787 -> 066 #5f8787
" 5f87af -> 067 #5f87af
" 6c6c6c -> 242 #6c6c6c
" 808080 -> 244 #808080
" 87ffaf -> 121 #87ffaf
" 949494 -> 246 #949494
" afd5ff -> 153 #afd7ff // the color of a highlighted text
" afafd7 -> 146 #afafd7
" bcbcbc -> 250 #bcbcbc
" ff0000 -> 196 #ff0000
